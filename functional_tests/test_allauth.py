import time
import logging

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .base import FunctionalTest

SIGNIN_ID = 'id_login'
LOGOUT_ID = 'id_logout'

logger = logging.getLogger(__name__)


class GoogleSigninTest(FunctionalTest):

    fixtures = ['allauth_fixture2']

    def get_button_by_id(self, element_id):
        return self.browser.wait.until(EC.element_to_be_clickable(
            (By.ID, element_id)
        ))

    def user_login(self):
        import json
        with open("expenses/fixtures/google_user.json") as f:
            credentials = json.loads(f.read())
        email = credentials["Email"]
        pw = credentials["Passwd"]
        self.get_element_by_id("Email").send_keys(email)
        self.get_element_by_id("next").click()
        self.get_element_by_id("Passwd").send_keys(pw)
        for btn in ["signIn", "submit_approve_access"]:
            time.sleep(3)
            self.get_element_by_id(btn).click()

        return

    def logout_user(self):
        self.take_screenshot("logout.png")
        self.browser.find_element_by_class_name("fa-bars").click()
        self.browser.find_elements_by_id(LOGOUT_ID)[0]
        element = WebDriverWait(self.browser, 6).until(
            EC.element_to_be_clickable((By.ID, LOGOUT_ID))
        )
        time.sleep(2)
        element.click()
        self.take_screenshot("logout1.png")
        self.browser.find_element_by_tag_name("button").click()

    def test_user_can_login(self):
        self.browser.get(self.server_url)
        self.browser.find_elements_by_id(SIGNIN_ID)[0].click()
        self.user_login()
        self.wait_for_element_with_id(LOGOUT_ID)
        # Check page is pending approval
        print('on pending approval page')
        self.assertIn('Pending Approval', self.browser.title)

        # User is approved by admin

        self.logout_user()
        self.wait_for_element_with_id(SIGNIN_ID)
