from decimal import Decimal
import re
import time

from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC

HOME_TOTAL_SPEND = 'total'
# ADD EXPENSE PAGE
ADD_EXPENSE_LOCATION_ID = 'id_location'
ADD_EXPENSE_COST_ID = 'id_cost'
ADD_EXPENSE_DATE_ID = 'id_transaction_date'
ADD_EXPENSE_CATEGORY_ID = 'id_category'
ADD_EXPENSE_SHARE_ID = 'id_share_with'
ADD_EXPENSE_HEADING = 'id_add_expense_heading'
# LIST EXPENSE PAGE
EXPENSE_TABLE = 'id-expense-table'
# COMMON
NOTIFICATION_ID = 'notification'


class BasePage(object):

    def __init__(self, test):
        self.test = test

    def open_nav(self):
        self.test.browser.find_element_by_class_name("fa-bars").click()
        time.sleep(2)

        # self.browser.find_elements_by_id(LOGOUT_ID)[0]
        # element = WebDriverWait(self.browser, 6).until(
        #     EC.element_to_be_clickable((By.ID, LOGOUT_ID))
        # )
    def go_to_expense_list_page(self):
        self.open_nav()
        self.test.browser.find_element_by_id("expense-list").click()
        self.test.wait_for(lambda: self.test.assertEqual(
            self.test.browser.find_element_by_class_name('page-header').text,
            'All Expenses'
        ))
        return ListExpensePage(self.test)

    def go_to_add_expense_page(self):
        self.open_nav()
        self.test.browser.find_element_by_id("add-expense").click()
        # self.test.take_screenshot('add_expense.png')
        self.test.wait_for(lambda: self.test.assertEqual(
            self.test.browser.find_element_by_id(ADD_EXPENSE_HEADING).text,
            'Add An Expense'
        ))
        return AddExpensePage(self.test)

    def to_home_page(self):
        self.test.browser.get(self.test.server_url)
        return HomePage(self.test)


class HomePage(BasePage):

    # refactor to login and go to home page
    # rename to login
    def go_to_home_page(self, username="tim@tim.com"):
        self.test.create_pre_authenticated_session(username)
        self.test.browser.get(self.test.server_url)
        self.test.wait_for_element_with_id('id_logout')
        self.test.wait_for(self.get_summary)

    def get_summary(self):
        return self.test.browser.find_element_by_class_name(HOME_TOTAL_SPEND)


class AddExpensePage(BasePage):

    def get_notification_text(self):
        return self.test.browser.find_element_by_id(NOTIFICATION_ID).text

    def get_location_input(self):
        return self.test.browser.find_element_by_id(ADD_EXPENSE_LOCATION_ID)

    def get_cost_input(self):
        return self.test.browser.find_element_by_id(ADD_EXPENSE_COST_ID)

    def get_category_input(self):
        return self.test.browser.find_element_by_id(ADD_EXPENSE_CATEGORY_ID)

    def get_date_input(self):
        return self.test.browser.find_element_by_id(ADD_EXPENSE_DATE_ID)

    def select_sharee(self, share_with):
        sharee = "//select[@id='id_share_with']/option[text()='%s']"
        sharee = sharee % share_with
        self.test.browser.find_element_by_xpath(sharee).click()

    def add_expense(self, location, category, date, cost, share_with=None):
        inputbox_location = self.get_location_input()
        inputbox_cost = self.get_cost_input()
        inputbox_category = self.get_category_input()
        inputbox_date = self.get_date_input()

        inputbox_location.send_keys(location)
        inputbox_cost.send_keys(cost)
        inputbox_date.send_keys(date)
        inputbox_category.send_keys(category)
        if share_with:
            self.select_sharee(share_with)

        # when she hits submit the page updates to show her expense in a list
        submit_btn = self.test.browser.find_element_by_id('id-submit-form')
        submit_btn.click()


class ListExpensePage(BasePage):

    def check_item_in_table(self, row_text):
        table = self.test.browser.find_element_by_id(EXPENSE_TABLE)
        rows = table.find_elements(By.CSS_SELECTOR, 'tbody tr')
        self.test.assertIn(row_text, [row.text.split(' $')[0] for row in rows])

    def get_first_expense(self, expense_data):
        table = self.test.browser.find_element_by_css_selector(
            '#id-expense-table tbody')
        rows = table.find_elements_by_tag_name('tr')
        cells = rows[0].find_elements_by_tag_name('td')
        location = cells[0].text
        cost = Decimal(re.sub('[$]', '', cells[1].text))
        share_with = cells[2].get_attribute('textContent')
        row_values = {
            'location': location,
            'cost': cost,
            'share_with': share_with
        }
        return row_values

    # Remove this function
    def find_expense(self, expense_data):
        table = self.test.browser.find_element_by_css_selector(
            '#id-expense-table tbody')
        rows = table.find_elements_by_tag_name('tr')
        for row in rows:
            cells = row.find_elements_by_tag_name('td')
            location = cells[0].text
            cost = Decimal(re.sub('[$]', '', cells[1].text))
            share_with = cells[2].get_attribute('textContent')
            print(location, cost, share_with)
            if (location == expense_data['location'] and
                    cost == Decimal(expense_data['cost'])/2 and
                    share_with == expense_data['share_with']):
                row_values = {
                    'location': location,
                    'cost': cost,
                    'share_with': share_with
                }
        return row_values
