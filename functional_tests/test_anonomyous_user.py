from selenium import webdriver

from .base import FunctionalTest


class AnonomyousUserTest(FunctionalTest):

    def test_anonomyous_user_redirected_to_landing_page(self):
        # anonomyous user hits home page
        self.browser.get(self.server_url)

        # user is redirected to landing page

        self.assertIn('Landing Page', self.browser.title)
        # user goes to expenses page


        url = self.server_url + '/expenses/' 
        self.browser.get(url)
        self.assertIn('Landing Page', self.browser.title)

        url = self.server_url + '/add-expense/' 
        self.browser.get(url)
        self.assertIn('Landing Page', self.browser.title)

