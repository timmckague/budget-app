import re
from decimal import Decimal

from selenium import webdriver

from .base import FunctionalTest
from .pages import HomePage


def quit_if_possible(browser):
    try:
        browser.quit()
    except:
        pass


class AddExpenseTest(FunctionalTest):

    def check_item_in_table(self, expense):
        expense_row = ' '.join(expense)
        expense_row = re.sub('[$]', '', expense_row)
        table = self.browser.find_element_by_id('id-recently-added')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(expense_row, [row.text for row in rows])

    def check_item_not_in_table(self, expense):
        expense_row = ' '.join(expense)
        expense_row = re.sub('[$]', '', expense_row)
        table = self.browser.find_element_by_id('id-recently-added')
        rows = table.find_elements_by_tag_name('tr')
        self.assertNotIn(expense_row, [row.text for row in rows])

    def check_place_holder_text(self, form_input, place_holder_text):
        self.assertEqual(
            form_input.get_attribute('placeholder'),
            place_holder_text
        )

    def test_user_can_add_expense(self):
        # Edith has heard about a cool new online to-do app. She goes
        # to check out its homepage
        HomePage(self).go_to_home_page("tim@mckague.com")

        # She decides to go add an expense
        expense_page = HomePage(self).go_to_add_expense_page()

        # she notices a form on the page and is invited to add an expense
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Add An Expense', header_text)

        # she notices a form on the page with two input boxes
        # the first field is for entering the location of an expense
        self.check_place_holder_text(
            expense_page.get_location_input(),
            'Enter location'
        )

        # the second field is for entering the cost of an expense
        self.check_place_holder_text(expense_page.get_cost_input(), '0.00')

        self.check_place_holder_text(
            expense_page.get_date_input(),
            'Enter Date'
        )

        self.check_place_holder_text(
            expense_page.get_category_input(),
            'Select Category'
        )

        # in the location field she enters Loblaws
        # she types 20.4 into the cost field
        # she enters the date of 2015-09-01
        # and the category is Grocery
        expense_data = [{'location': 'Loblaws',
                         'cost': '20.4',
                         'date': '2015-09-01',
                         'category': 'GR'}]

        expense_page.add_expense(**expense_data[0])
        # after submitting the form she notices the recently added section
        # has updated to contain her most recent entry
        self.assertEqual(
            expense_page.get_notification_text(),
            'Success, new Grocery expense: Loblaws, $20.40'
        )

        # There is still a text box to enter more expenses
        # in the location field she enters LCBO
        # she types 30.67 into the cost field
        expense_data = [{'location': 'LCBO',
                         'cost': '30.67',
                         'date': '2015-09-02',
                         'category': 'AL'}]

        expense_page.add_expense(**expense_data[0])
        self.assertEqual(
            expense_page.get_notification_text(),
            'Success, new Alcohol expense: LCBO, $30.67'
        )

    def test_user_can_share_an_expense(self):
        HomePage(self).go_to_home_page("tim@mckague.com")
        tim_browser = self.browser
        self.addCleanup(lambda: quit_if_possible(tim_browser))
        # decides to go add an expense

        # Victoria is also hanging out on the site
        tor_browser = webdriver.Firefox()
        self.addCleanup(lambda: quit_if_possible(tor_browser))
        self.browser = tor_browser
        HomePage(self).go_to_home_page("victoria@mail.com")

        # Tim goes to the expense page
        self.browser = tim_browser
        expense_page = HomePage(self).go_to_add_expense_page()
        expense_data = [{'location': 'Loblaws',
                         'cost': '20.4',
                         'date': '2015-09-01',
                         'category': 'GR',
                         'share_with': 'victoria@mail.com'}]
        expense_page.add_expense(**expense_data[0])
        # after submitting the form she notices the recently added section
        # has updated to contain her most recent entry
        self.assertEqual(
            expense_page.get_notification_text(),
            'Success, new Grocery expense: Loblaws, $20.40'
        )

        # Tim navigates to his expense list page
        expense_list_page = HomePage(self).go_to_expense_list_page()

        # He sees the expense he just added in his list of expenses
        expense_details = expense_list_page.get_first_expense(expense_data[0])

        # He can see that it is a shared expense with Victoria
        self.assertEquals(expense_details['share_with'], 'victoria@mail.com')
        # He can see that the cost is split 50:50
        self.assertEquals(expense_details['cost'], Decimal('10.2'))

        # Victoria now goes to her expense's page
        self.browser = tor_browser
        tor_expense_list_page = HomePage(self).go_to_expense_list_page()

        # she can see the expense that Tim entered
        expense_details = tor_expense_list_page.get_first_expense(expense_data[0])

        # she can see that it is a shared expense with Tim
        self.assertEquals(expense_details['share_with'], 'tim@mckague.com')
        # she can see that she is charged for 50% of the total
        self.assertEquals(expense_details['cost'], Decimal('10.2'))
