from .pages import HomePage
from .base import FunctionalTest


class TestExpensesPage(FunctionalTest):

    def test_user_can_view_all_expenses(self):

        # Edith has heard about a cool new online to-do app. She goes
        # to check out its homepage
        HomePage(self).go_to_home_page("tim@mckague.com")

        # She decides to go add some expenses
        expense_page = HomePage(self).go_to_add_expense_page()

        expense_data = [{'location': 'Loblaws',
                         'cost': '80.4',
                         'date': '2015-10-02',
                         'category': 'GR'}]

        expense_page.add_expense(**expense_data[0])

        expense_data = [{'location': 'Gap',
                         'cost': '50',
                         'date': '2015-10-01',
                         'category': 'CL'}]

        expense_page.add_expense(**expense_data[0])

        expense_data = [{'location': 'LCBO',
                         'cost': '20.94',
                         'date': '2015-09-28',
                         'category': 'AL'}]

        expense_page.add_expense(**expense_data[0])

        expense_data = [{'location': 'Fynns Temple Bar',
                         'cost': '10.4',
                         'date': '2015-10-01',
                         'category': 'AL'}]

        expense_page.add_expense(**expense_data[0])

        # She wants to see a table showing all her past expenses
        # notices in the menu a link that says "EXPENSES", she clicks it
        expense_list_page = expense_page.go_to_expense_list_page()
        self.assertRegex(self.browser.current_url, '/expenses/')

        # She notices the page title and header mention Expenses
        self.assertIn('Expenses', self.browser.title)

        # She see a table that contains all her expenses
        expense_list_page.check_item_in_table('Loblaws')
        expense_list_page.check_item_in_table('Gap')
        expense_list_page.check_item_in_table('LCBO')
        expense_list_page.check_item_in_table('Fynns Temple Bar')
