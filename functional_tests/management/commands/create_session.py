from django.contrib.auth.models import Permission
from django.conf import settings
from django.contrib.auth import (
    BACKEND_SESSION_KEY, SESSION_KEY,
    get_user_model, HASH_SESSION_KEY
)
from django.contrib.sessions.backends.db import SessionStore
from django.core.management.base import BaseCommand

User = get_user_model()


class Command(BaseCommand):

    def handle(self, email, *_, **__):
        session_key = create_pre_authenticated_session(email)
        self.stdout.write(session_key)


def create_pre_authenticated_session(email):
    user = User.objects.create(username=email)
    permission = Permission.objects.get(name='Can use site')
    user.user_permissions.add(permission)
    user.save()
    session = SessionStore()
    session[SESSION_KEY] = user.pk
    session[HASH_SESSION_KEY] = user.get_session_auth_hash()
    session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[1]
    session.save()
    return session.session_key
