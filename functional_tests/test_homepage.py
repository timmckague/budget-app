from .base import FunctionalTest
from .pages import HomePage


class TestHomePage(FunctionalTest):

    def test_user_can_view_home_page(self):

        # Edith has heard about a cool new online to-do app. She goes
        # to check out its homepage
        HomePage(self).go_to_home_page("tim@mckague.com")

        # She notices the page title and header mention Expenses
        self.assertIn('Expense Summary', self.browser.title)

    def test_total_reflects_all_expenses(self):
        pass

    def test_sub_categories_reflect_actual_spend(self):
        pass
