# Expense Tracker



## Provision/Deploy
### Setup Local Environment
1. Install virtual box, vagrant and ansible
	* ansible: $ sudo pip install ansible
2. Install vagrant guest: $ vagrant plugin install vagrant-vbguest
3. provision $ vagrant up local
4. create secret.txt
5. vagrant ssh local
6. run server: 
	$ cd ~/sites
	$ source virtualenv/bin/activate
	$ cd source
	$ python manage.py makemigrations
	$ python manage.py migrate
	$ python manage.py runserver 0.0.0.0:8000
7. add site "local.expensetracker.com"
8. setup allauth
9. $ python manage.py test

### Creating Fixtures
1. $ python manage.py dumpdata --indent 2 --natural-foreign --exclude auth.permission --exclude contenttypes > _fixturefilename_
2. Use a user without view site perms for all\_auth test

### Set Staging Environment


### Deploy to Openshift
1. ensure env variable "DJANGO\_SECRET" is set
	$ rhc set env DJANGO\_SECRET=Somesecret -a app-name








