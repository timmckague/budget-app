"""
Django settings for budget_app project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

ON_OPENSHIFT = False
if 'OPENSHIFT_REPO_DIR' in os.environ:
    ON_OPENSHIFT = True

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "expenses/templates/")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                # Required by allauth template tags
                "django.template.context_processors.request",

                # allauth specific context processors
                # "allauth.account.context_processors.account",
                # "allauth.socialaccount.context_processors.socialaccount",
            ],
        },
    },
]
if ON_OPENSHIFT:
    SECRET_KEY = os.environ['DJANGO_SECRET']
    DEBUG = False
    # TEMPLATE_DEBUG = False
    from socket import gethostname
    ALLOWED_HOSTS = [
        'ex-std-node.prod.rhcloud.com',
        os.environ['OPENSHIFT_APP_DNS'],
        gethostname()
    ]
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(
                os.environ['OPENSHIFT_DATA_DIR'], 'sqlite3.db'
            ),
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }
    WSGI_APPLICATION = 'budget_app.wsgi.application'
    STATIC_ROOT = os.path.join(
            os.environ.get('OPENSHIFT_REPO_DIR'),
            'wsgi',
            'static'
    )
else:
    with open('secret_key.txt') as f:
        SECRET_KEY = f.read().strip()

    DEBUG = True
    # TEMPLATE_DEBUG = True
    ALLOWED_HOSTS = []
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'budgetapp',
            'USER': 'django',
            'PASSWORD': 'secretpassword',
            'HOST': 'localhost',
            'PORT': '',
        }
    }
    WSGI_APPLICATION = 'budget_app.wsgi.application'
    STATIC_ROOT = os.path.join(BASE_DIR, '../static')


AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
)

# TEMPLATE_DIRS = (
#     os.path.join(BASE_DIR, "expenses/templates/"),
# )

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    # needed for all-auth
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # needed for allauth
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.google',
    # app specific apps
    'expenses',

    # form rendering help
    'widget_tweaks',
)

SITE_ID = 2


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'budget_app.urls'

LOGGING = {
   'version': 1,
   'disable_existing_loggers': False,
   'handlers': {
       'console': {
           'level': 'DEBUG',
           'class': 'logging.StreamHandler',
       },
   },
   'loggers': {
        'django': {
            'handlers': ['console'],
        },
        'accounts': {
            'handlers': ['console'],
        },
        'expenses': {
            'handlers': ['console'],
        },
    },
    'root': {'level': 'INFO'},
}

MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'

# Internationalization
# Internationalization https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Test Fixtures
# FIXTURE_DIRS = (
#     os.path.join(BASE_DIR, 'expenses', 'fixtures'),
# )

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'budget_app', 'static'),
    # os.path.join(BASE_DIR, 'expenses', 'static'),
    # os.path.join(BASE_DIR, 'accounts', 'static'),
)
STATIC_URL = '/static/'


# login
LOGIN_URL = "/landing/"

# allauth configuration
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_EMAIL_VERIFICATION = "none"
SOCIALACCOUNT_QUERY_EMAIL = True
LOGIN_REDIRECT_URL = "/"
ACCOUNT_LOGOUT_REDIRECT_URL = "/landing/"
