from django.conf.urls import patterns, include, url
from django.contrib import admin
from expenses import views

urlpatterns = patterns('',
    url(r'^$', views.home_page, name='home_page'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^expenses/$', views.expense_list_page, name='expense_list_page'),
    url(r'^add-expense/$', views.add_expense, name='add_expense'),
    url(r'^landing/$', views.landing_page, name='landing_page'),
    url(r'^approval/$', views.approval_page, name='approval_page'),
)
