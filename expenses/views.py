import logging

# from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.dispatch import receiver
from allauth.account.signals import user_signed_up

from expenses.models import Expense
from expenses.forms import ExpenseForm

logger = logging.getLogger(__name__)

@permission_required('expenses.view_site', login_url='/approval/')
@login_required
def add_expense(request):
    if request.method == 'POST':
        form = ExpenseForm(user=request.user, data=request.POST)
        value = form.is_valid()
        if form.is_valid():
            form.save(request=request)
            return redirect('/add-expense/')
    else:
        form = ExpenseForm(initial={'cost':''})

    return render(request, 'new_expense.html', {
        'title': "Add Expense",
        'form': form,
    })

@permission_required('expenses.view_site', login_url='/approval/')
@login_required
def expense_list_page(request):
    expenses = Expense.get_expenses(request.user)
    return render(request, 'expense_list.html', {
        'title': "Expenses",
        'expenses': expenses,
    })

@permission_required('expenses.view_site', login_url='/approval/')
@login_required
def home_page(request):
    return render(request, 'summary.html', {
        'title': "Expense Summary",
    })

@login_required
def approval_page(request):
    return render(request, 'approval.html', {
        'title': "Pending Approval",
    })

def landing_page(request):
    return render(request, 'landing.html', {
        'title': "Landing Page",
    })


# @receiver(user_signed_up)
# def user_signed_up(request, user, **kwargs):
    # user.is_active = False
    # user.save()
