# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('expenses', '0004_expense_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='expense',
            name='share_with',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, related_name='shared_expenses'),
            preserve_default=True,
        ),
    ]
