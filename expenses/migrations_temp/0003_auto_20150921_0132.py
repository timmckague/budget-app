# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('expenses', '0002_auto_20150621_0106'),
    ]

    operations = [
        migrations.AddField(
            model_name='expense',
            name='category',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='expense',
            name='transaction_date',
            field=models.DateField(default=datetime.datetime.now),
            preserve_default=True,
        ),
    ]
