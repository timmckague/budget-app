from datetime import datetime
import logging

from django.conf import settings
from django.db import models

CATEGORY_OPTIONS = (
        ('GR', 'Grocery'),
        ('RT', 'Restaurant'),
        ('AL', 'Alcohol'),
        ('TR', 'Transport'),
        ('CL', 'Clothing'),
        ('PC', 'Personal Care'),
        ('FN', 'Fun'),
        ('MI', 'Misc'),
        ('GI', 'Gift'),
        ('VA', 'Vacation'),
        ('LV', 'Living')
)

logger = logging.getLogger(__name__)


class Expense(models.Model):
    location = models.TextField(default='')
    cost = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    category = models.CharField(
        default='MI', choices=CATEGORY_OPTIONS, max_length=2
    )
    transaction_date = models.DateField(default=datetime.now)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    share_with = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name='shared_expenses'
    )
    payer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='payer'
    )
    description = models.TextField(default='', blank=True)

    class Meta:
        permissions = (
            ("view_site", "Can use site"),
        )

    def get_effective_cost(self):
        if self.share_with:
            effective_cost = self.cost / 2
        else:
            effective_cost = self.cost
        return effective_cost

    @staticmethod
    def create_new(
        location, cost, transaction_date, category,
        owner, share_with=None, payer=None, description=''
    ):
        if not payer:
            payer = owner
        expense_ = Expense.objects.create(
                location=location,
                cost=cost,
                transaction_date=transaction_date,
                category=category,
                owner=owner,
                share_with=share_with,
                payer=payer
        )
        return expense_

    def get_expenses(owner):
        values = Expense.objects.filter(
            models.Q(owner=owner) | models.Q(share_with=owner)
        ).all()
        return values
