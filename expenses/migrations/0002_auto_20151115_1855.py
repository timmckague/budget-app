# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('expenses', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='expense',
            options={'permissions': (('view_site', 'Can use site'),)},
        ),
        migrations.AlterField(
            model_name='expense',
            name='category',
            field=models.CharField(max_length=2, choices=[('GR', 'Grocery'), ('RT', 'Restaurant'), ('AL', 'Alcohol'), ('TR', 'Transport'), ('CL', 'Clothing'), ('PC', 'Personal Care'), ('FN', 'Fun'), ('MI', 'Misc'), ('GI', 'Gift'), ('VA', 'Vacation'), ('LV', 'Living')], default='MI'),
        ),
    ]
