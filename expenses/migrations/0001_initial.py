# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('location', models.TextField(default='')),
                ('cost', models.DecimalField(default=0, decimal_places=2, max_digits=9)),
                ('category', models.TextField(default='')),
                ('transaction_date', models.DateField(default=datetime.datetime.now)),
                ('owner', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL)),
                ('share_with', models.ForeignKey(null=True, blank=True, related_name='shared_expenses', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
