import datetime
from decimal import Decimal
import unittest

from django.test import TestCase
from allauth.utils import get_user_model

from expenses.models import Expense

User = get_user_model()


class ExpenseModelTest(TestCase):

    def setUp(self):
        self.owner = User.objects.create(username="tim")
        self.data = {
            'location': 'Loblaws',
            'cost': Decimal('50.3'),
            'transaction_date': datetime.date(2015, 10, 14),
            'category': 'GR',
            'owner': self.owner,
            'share_with': None,
            'payer': None,
            'description': ''
        }

    def create_expense(self, data=None):
        if not data:
            data = self.data

        Expense.create_new(
            location=data['location'],
            cost=data['cost'],
            transaction_date=data['transaction_date'],
            category=data['category'],
            owner=data['owner'],
            share_with=data['share_with'],
            payer=data['payer'],
            description=data['description']
        )

    def check_expense(self, expense_model, expense_data, payer):
        self.assertEqual(
                expense_model.transaction_date,
                expense_data['transaction_date']
        )
        self.assertEqual(expense_model.cost, expense_data['cost'])
        self.assertEqual(expense_model.category, expense_data['category'])
        self.assertEqual(expense_model.location, expense_data['location'])
        self.assertEqual(expense_model.owner, expense_data['owner'])
        self.assertEqual(expense_model.share_with, None)
        self.assertEqual(expense_model.description, '')
        self.assertEqual(expense_model.payer, payer)

    def test_create_new_saves_when_required_field_populated(self):
        self.create_expense()
        new_expense = Expense.objects.first()
        self.check_expense(new_expense, self.data, payer=self.data['owner'])

    def test_create_new_saves_description(self):
        self.data['description'] = 'a party'
        self.create_expense()
        new_expense = Expense.objects.first()
        self.check_expense(new_expense, self.data, payer=self.data['owner'])

    def test_create_new_saves_shared_owner_is_payer(self):
        friend = User.objects.create(username="friend@email.com")
        self.data['share_with'] = friend
        self.create_expense()
        new_expense = Expense.objects.first()
        self.assertEqual(new_expense.payer, self.owner)
        self.assertEqual(new_expense.share_with, friend)

        # new_expense = Expense.objects.first()
        # self.check_expense(new_expense, self.data)

    def test_create_new_saves_shared_sharee_is_payer(self):
        friend = User.objects.create(username="friend@email.com")
        self.data['share_with'] = friend
        self.data['payer'] = friend
        self.create_expense()
        new_expense = Expense.objects.first()
        self.assertEqual(new_expense.payer, friend)
        self.assertEqual(new_expense.share_with, friend)

    @unittest.skip('save not overriden')
    def test_clean_is_called_whening_creating_expense(self):
        friend = User.objects.create(username="friend@email.com")
        self.data['share_with'] = None
        self.data['payer'] = friend
        self.assertRaises(ValidationError, Expense.create_new, **self.data)

    @unittest.skip('save not overriden')
    def test_clean_throws_error_payer_DNE_owner_when_not_shared(self):
        friend = User.objects.create(username="friend@email.com")
        self.data['payer'] = friend
        self.data['share_with'] = None
        expense_ = Expense(**self.data)
        self.assertRaises(ValidationError, expense_.clean)

    def test_get_expenses_returns_owned_expenses(self):
        self.create_expense()
        new_expense = Expense.get_expenses(self.owner)[0]
        self.check_expense(new_expense, self.data, self.owner)

    def test_get_expenses_doesnt_return_owned_expenses_for_other_users(self):
        self.create_expense()

        victoria = User.objects.create(username="victoria")

        victoria_expenses_count = len(Expense.get_expenses(victoria))
        self.assertEqual(victoria_expenses_count, 0)

    def test_get_expenses_returns_shared_expenses(self):
        friend = User.objects.create(username="friend@email.com")
        self.data['share_with'] = friend
        self.create_expense()

        friend_expenses_count = len(Expense.get_expenses(friend))
        self.assertEqual(friend_expenses_count, 1)
