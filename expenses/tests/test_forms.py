import datetime
from decimal import Decimal
import unittest
from unittest.mock import Mock, patch

from django.core.exceptions import ValidationError
from allauth.utils import get_user_model
from django.http import HttpRequest

from expenses.forms import ExpenseForm, SUCCESS_MSG

User = get_user_model()


class ExpenseFormTest(unittest.TestCase):

    @patch('expenses.forms.Expense.create_new')
    def test_save_creates_new_expense_from_post_data_if_user_authenticated(
        self, mock_Expense_create_new
    ):
        user = Mock()
        mock_Expense_create_new.return_value.cost = Decimal('50.3')
        mock_request = Mock()
        mock_request.user.return_value = user
        # mock_request.user.is_authenticated.return_value = True
        mock_messages = patch('expenses.forms.messages').start()
        form = ExpenseForm(user=user,
                           data={'location': 'Loblaws',
                                 'cost': '50.3',
                                 'transaction_date': '2015-10-14',
                                 'category': 'GR'})
        form.is_valid()
        form.save(request=mock_request)
        mock_Expense_create_new.assert_called_once_with(
            location='Loblaws',
            cost=Decimal('50.3'),
            transaction_date=datetime.date(2015, 10, 14),
            category='GR',
            owner=user,
            share_with=None,
            payer=user,
            description=''
        )
        mock_messages.return_value

    @unittest.skip("whether user is authenticated is handled by view")
    @patch('expenses.forms.Expense.create_new')
    def test_save_doesnt_create_new_expense_from_post_data_not_authenticated(
        self, mock_Expense_create_new
    ):
        user = Mock()
        mock_Expense_create_new.return_value.cost = Decimal('50.3')
        mock_request = Mock()
        mock_request.user.return_value = user
        # mock_request.user.is_authenticated.return_value = False
        mock_messages = patch('expenses.forms.messages').start()
        form = ExpenseForm(user=user,
                           data={'location': 'Loblaws',
                                 'cost': '50.3',
                                 'transaction_date': '2015-10-14',
                                 'category': 'GR'})
        form.is_valid()
        form.save(request=mock_request)
        self.assertFalse(mock_Expense_create_new.called)
        mock_messages.return_value

    @patch('expenses.forms.Expense.create_new')
    def test_passes_success_message_on_save(
        self, mock_Expense_create_new
    ):
        user = Mock()
        mock_Expense_create_new.return_value.cost = Decimal('50.3')
        mock_request = Mock()
        mock_request.user.return_value = user
        # mock_request.user.is_authenticated.return_value = True
        mock_Expense_create_new.return_value.cost = Decimal('50.3')
        mock_Expense_create_new         \
            .return_value               \
            .get_category_display       \
            .return_value = 'Grocery'
        mock_Expense_create_new.return_value.location = 'Loblaws'
        mock_messages = patch('expenses.forms.messages').start()
        data = {'location': 'Loblaws',
                'cost': '50.3',
                'transaction_date': '2015-10-14',
                'category': 'GR'}
        form = ExpenseForm(user=user, data=data)
        form.is_valid()
        form.save(request=mock_request)

        msg = SUCCESS_MSG.format(
            location=form.cleaned_data['location'],
            category='Grocery',
            cost=form.cleaned_data['cost']
        )
        mock_messages.success.assert_called_once_with(mock_request, msg)

    def test_form_invalid_when_sharing_with_nonexistant_user(self):
        pass

    @patch('expenses.forms.Expense.create_new')
    def test_save_creates_new_expense_when_sharing_with_user(
        self, mock_Expense_create_new
    ):
        mock_Expense_create_new.return_value.cost = Decimal('50.3')
        # user = Mock(is_authenticated=lambda: True)
        user = Mock()
        friend = User.objects.create(username='friend@email.com')
        mock_request = Mock()
        mock_request.user.return_value = user

        mock_messages = patch('expenses.forms.messages').start()

        form = ExpenseForm(user=user,
                           data={'location': 'Loblaws',
                                 'cost': '50.3',
                                 'transaction_date': '2015-10-14',
                                 'category': 'GR',
                                 'share_with': friend.id,
                                 'payer': friend.id})

        form.is_valid()
        form.save(request=mock_request)
        mock_Expense_create_new.assert_called_once_with(
            location='Loblaws',
            cost=Decimal('50.3'),
            transaction_date=datetime.date(2015, 10, 14),
            category='GR',
            owner=user,
            share_with=friend,
            payer=friend,
            description=''
        )
        mock_messages.return_value
        friend.delete()

    def test_is_valid_calls_form_clean(self):
        # user = Mock(is_authenticated=lambda: True)
        user = Mock()
        user.pk.return_value = 1
        # mock_Expense_create_new.return_value.cost = Decimal('50.3')
        friend = User.objects.create(username='friend1@email.com')
        # request = HttpRequest()
        # mock_messages = patch('expenses.forms.messages').start()

        form = ExpenseForm(user=user,
                           data={'location': 'Loblaws',
                                 'cost': '50.3',
                                 'transaction_date': '2015-10-14',
                                 'category': 'GR',
                                 'share_with': None,
                                 'payer': friend.id})

        value = form.is_valid()
        self.assertFalse(value)
        # self.assertRaises(ValidationError, form.is_valid)
