from decimal import Decimal
from importlib import import_module
import unittest
from unittest.mock import patch

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.http import HttpRequest
from django.core.urlresolvers import resolve
from django.conf import settings
from django.contrib.auth import login
from django.test.client import Client
from allauth.utils import get_user_model

from expenses.forms import SUCCESS_MSG
from expenses.models import Expense
from expenses.views import (
        landing_page, home_page, expense_list_page,
        add_expense, approval_page
)

# form ids
ADD_EXPENSE_LOCATION = 'id_location'
ADD_EXPENSE_COST = 'id_cost'
ADD_EXPENSE_TRANSACTION_DATE = 'id_transaction_date'
ADD_EXPENSE_CATEGORY = 'id_category'
ADD_EXPENSE_SHARE = 'id_share'

User = get_user_model()


class BaseTestCase(TestCase):

    def login(self, user=None, perm=False):
        client = TestClient()
        if not user:
            user = User.objects.create(username="tim")
        if perm:
            permission = Permission.objects.get(name='Can use site')
            user.user_permissions.add(permission)
        client.login_user(user)
        return client

    def create_user(self, name, perm=False):
        user = User.objects.create(username=name)
        if perm:
            permission = Permission.objects.get(name='Can use site')
            user.user_permissions.add(permission)
        return user


class TestClient(Client):

    def login_user(self, user):
        """
        Login as specified user, does not depend on auth backend (hopefully)
        This is based on Client.login() with a small hack that does not
        require the call to authenticate()
        """

        if 'django.contrib.sessions' not in settings.INSTALLED_APPS:
            raise AssertionError(
                "Unable to login without django.contrib.sessions" +
                " in INSTALLED_APPS")
        user.backend = "%s.%s" % (
            "django.contrib.auth.backends",
            "ModelBackend"
        )
        engine = import_module(settings.SESSION_ENGINE)
        # Create a fake request to store login details.
        request = HttpRequest()

        # if self.session:
        #    request.session = self.session
        # else:
        #    request.session = engine.SessionStore()
        from django.contrib.messages.storage.cookie import CookieStorage
        setattr(request, 'session', engine.SessionStore())
        messages = CookieStorage(request)
        request._messages = messages
        login(request, user)
        # Set the cookie to represent the session.
        session_cookie = settings.SESSION_COOKIE_NAME
        self.cookies[session_cookie] = request.session.session_key
        cookie_data = {
            'max-age': None,
            'path': '/',
            'domain': settings.SESSION_COOKIE_DOMAIN,
            'secure': settings.SESSION_COOKIE_SECURE or None,
            'expires': None,
        }
        self.cookies[session_cookie].update(cookie_data)
        # Save the session values.
        request.session.save()


class LandingPageTest(TestCase):

    def __init__(self, *args, **kwargs):
        super(LandingPageTest, self).__init__(*args, **kwargs)
        self.url = "/landing/"
        self.template = "landing.html"
        self.title = "Landing Page"

    def test_landing_page_url_resolves(self):
        found = resolve(self.url)
        self.assertEqual(found.func, landing_page)

    def test_landing_page_title(self):
        response = self.client.get(self.url)
        self.assertEqual(self.title, response.context['title'])

    def test_landing_page_render_template(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, self.template)


class ApprovalPageTest(BaseTestCase):

    def __init__(self, *args, **kwargs):
        super(ApprovalPageTest, self).__init__(*args, **kwargs)
        self.url = "/approval/"
        self.template = "approval.html"
        self.title = "Pending Approval"

    def test_root_url_resolves(self):
        found = resolve(self.url)
        self.assertEqual(found.func, approval_page)

    def test_redirects_for_anonymous_users(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, '/landing/?next=' + self.url)

    def test_approval_title(self):
        client = self.login()
        response = client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.title, response.context['title'])

    def test_approval_render_template(self):
        client = self.login()
        response = client.get(self.url)
        self.assertTemplateUsed(response, self.template)


class HomePageTest(BaseTestCase):

    def __init__(self, *args, **kwargs):
        super(HomePageTest, self).__init__(*args, **kwargs)
        self.url = "/"
        self.template = "summary.html"
        self.title = "Expense Summary"

    def test_root_url_resolves(self):
        found = resolve(self.url)
        self.assertEqual(found.func, home_page)

    def test_redirects_for_anonymous_users(self):
        response = self.client.get(self.url, follow=True)
        page_url = '/landing/?next=/approval/%3Fnext%3D%252F'
        self.assertRedirects(response, page_url)

    def test_redirects_for_unapproved_users(self):
        client = self.login()
        response = client.get(self.url)
        self.assertRedirects(response, '/approval/?next=' + self.url)

    def test_home_title(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.title, response.context['title'])

    def test_home_render_template(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        self.assertTemplateUsed(response, self.template)


@patch('expenses.views.ExpenseForm')
class AddExpenseViewUnitTest(unittest.TestCase):

    def setUp(self):
        self.request = HttpRequest()
        self.request.POST[ADD_EXPENSE_LOCATION] = 'Loblaw'
        self.request.POST[ADD_EXPENSE_COST] = '50.3'
        self.request.POST[ADD_EXPENSE_TRANSACTION_DATE] = '2015-10-09'
        self.request.POST[ADD_EXPENSE_CATEGORY] = 'GR'
        user = User.objects.create_user(username="expense_user")
        permission = Permission.objects.get(name='Can use site')
        user.user_permissions.add(permission)
        self.request.user = user

    def tearDown(self):
        self.request.user.delete()
        self.request = None

    def test_passes_POST_data_to_ExpenseForm(self, mockExpenseForm):
        mock_messages = patch('expenses.views.messages').start()
        self.request.method = 'POST'
        add_expense(self.request)
        mockExpenseForm.assert_called_once_with(user=self.request.user,
                                                data=self.request.POST)
        # calling to avoid pep8 errors...
        mock_messages.return_value

    def test_saves_form_with_owner_when_data_is_valid(self, mockExpenseForm):
        mock_form = mockExpenseForm.return_value
        mock_form.is_valid.return_value = True
        self.request.method = 'POST'
        add_expense(self.request)
        mock_form.save.assert_called_once_with(
            request=self.request
        )

    @patch('expenses.views.redirect')
    def test_redirects_to_form_if_object_is_valid(
            self, mock_redirect, mockExpenseForm
    ):
        mock_form = mockExpenseForm.return_value
        mock_form.is_valid.return_value = True
        mock_messages = patch('expenses.views.messages').start()

        self.request.method = 'POST'
        response = add_expense(self.request)

        self.assertEqual(response, mock_redirect.return_value)
        mock_redirect.assert_called_once_with('/add-expense/')
        # calling to avoid pep8 errors...
        mock_messages.return_value

    @patch('expenses.views.render')
    def test_reloads_form_with_data_on_invalid(
            self, mock_render, mockExpenseForm
    ):
        mock_form = mockExpenseForm.return_value
        mock_form.is_valid.return_value = False

        response = add_expense(self.request)

        self.assertEqual(response, mock_render.return_value)
        mock_render.assert_called_once_with(
            self.request, 'new_expense.html',
            {'title': 'Add Expense', 'form': mock_form}
        )

    def test_doesnt_save_if_form_invalid(self, mockExpenseForm):
        mock_form = mockExpenseForm.return_value
        mock_form.is_valid.return_value = False
        add_expense(self.request)
        self.assertFalse(mock_form.save.called)

    # TODO: test error messages on expense creation form
    def test_displays_error_messages(self, mockExpenseForm):
        pass


class AddExpenseViewIntegratedTest(BaseTestCase):

    def __init__(self, *args, **kwargs):
        super(AddExpenseViewIntegratedTest, self).__init__(*args, **kwargs)
        self.url = "/add-expense/"
        self.template = "new_expense.html"
        self.title = "Add Expense"
        self.data = {'location': 'Loblaws',
                     'cost': '40.99',
                     'transaction_date': '2015-09-09',
                     'category': 'GR'}

    def post_new_expense(self, data, owner, follow=False, client=None):
        if not client:
            client = self.login(owner)
        response = client.post(self.url, data, follow=follow)
        return response

    def test_renders_without_location_error_msg_on_get(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        form = response.context['form']
        self.assertFalse(form.has_error('location'))

    def test_renders_without_cost_error_msg_on_get(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        form = response.context['form']
        self.assertFalse(form.has_error('cost'))

    def test_renders_without_date_error_msg_on_get(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        form = response.context['form']
        self.assertFalse(form.has_error('transaction_date'))

    def test_renders_with_location_error_msg_on_missing(self):
        self.data['location'] = ''
        user = self.create_user('tim', True)

        response = self.post_new_expense(self.data, user, follow=True)

        form = response.context['form']
        self.assertTrue(form.has_error('location'))

    def test_renders_with_cost_error_msg_on_missing(self):
        self.data['cost'] = ''
        user = self.create_user('tim', True)

        response = self.post_new_expense(self.data, user, follow=True)

        form = response.context['form']
        self.assertTrue(form.has_error('cost'))

    def test_renders_with_date_error_msg_on_missing(self):
        self.data['transaction_date'] = ''
        user = self.create_user('tim', True)

        response = self.post_new_expense(self.data, user, follow=True)

        form = response.context['form']
        self.assertTrue(form.has_error('transaction_date'))

    def test_renders_success_message_on_valid_form(self):
        user = self.create_user('tim', True)

        response = self.post_new_expense(self.data, user, follow=True)
        messages = list(response.context['messages'])

        msg = SUCCESS_MSG.format(
            location=self.data['location'],
            category='Grocery',
            cost=Decimal(self.data['cost'])
        )
        self.assertEqual(msg, messages[0].message)

    def test_add_expense_url_resolves(self):
        found = resolve(self.url)
        self.assertEqual(found.func, add_expense)

    def test_add_expense_render_template(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        self.assertTemplateUsed(response, self.template)

    def test_redirects_for_unapproved_users(self):
        client = self.login()
        response = client.get(self.url)
        self.assertRedirects(response, '/approval/?next=' + self.url)

    def test_add_expense_redirects_for_anonymous_users(self):
        response = self.client.get(self.url, follow=True)
        page_url = '/landing/?next=/approval/%3Fnext%3D%252Fadd-expense%252F'
        self.assertRedirects(response, page_url)

    def test_add_expense_title(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        self.assertEqual(self.title, response.context['title'])

    def test_add_expense_only_save_when_posting(self):
        client = self.login(perm=True)
        client.get(self.url)
        self.assertEqual(Expense.objects.count(), 0)

    def test_add_expense_page_can_save_post(self):
        user = self.create_user('tim', True)

        response = self.post_new_expense(self.data, user)
        self.assertEqual(Expense.objects.count(), 1)
        saved_expense = Expense.objects.first()
        self.assertEqual(saved_expense.location, 'Loblaws')
        self.assertEqual(saved_expense.cost, Decimal('40.99'))
        self.assertEqual(saved_expense.category, 'GR')
        self.assertEqual(
            saved_expense.transaction_date.strftime('%Y-%m-%d'),
            '2015-09-09'
        )
        self.assertEqual(saved_expense.owner, user)

    def test_add_expense_page_redirects_after_post(self):
        user = self.create_user('tim', True)

        response = self.post_new_expense(self.data, user)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'http://testserver' + self.url)


class ExpenseListViewUnitTest(unittest.TestCase):

    def setUp(self):
        self.request = HttpRequest()
        self.request.method = 'GET'
        # if using this method must only called setUp once...
        user = User.objects.create(username="tim")
        permission = Permission.objects.get(name='Can use site')
        user.user_permissions.add(permission)
        self.request.user = user

    def tearDown(self):
        self.request.user.delete()
        self.request = None

    @patch('expenses.views.Expense.get_expenses')
    def test_loads_owners_expenses(self, mock_get_expenses):
        expense_list_page(self.request)
        mock_get_expenses.assert_called_once_with(
            self.request.user
        )

    @unittest.skip
    def test_displays_message_when_0_expenses_exist(self):
        pass


class ExpensesListViewIntegratedTest(BaseTestCase):

    def __init__(self, *args, **kwargs):
        super(ExpensesListViewIntegratedTest, self).__init__(*args, **kwargs)
        self.url = "/expenses/"
        self.template = "expense_list.html"
        self.title = "Expenses"

    def test_expense_list_url_resolves(self):
        found = resolve(self.url)
        self.assertEqual(found.func, expense_list_page)

    def test_list_render_template(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        self.assertTemplateUsed(response, self.template)

    def test_list_title(self):
        client = self.login(perm=True)
        response = client.get(self.url)
        self.assertEqual(self.title, response.context['title'])

    def test_redirects_for_unapproved_users(self):
        client = self.login()
        response = client.get(self.url)
        page_url = '/approval/?next=/expenses/'
        self.assertRedirects(response, page_url)

    def test_expenses_list_redirects_for_anonymous_users(self):
        response = self.client.get(self.url, follow=True)
        page_url = '/landing/?next=/approval/%3Fnext%3D%252Fexpenses%252F'
        self.assertRedirects(response, page_url)

    def test_expense_page_displays_all_expenses(self):
        user = self.create_user('tim', perm=True)
        client = self.login(user)

        data = {'location': 'Loblaws',
                'cost': '40.99',
                'transaction_date': '2015-09-09',
                'category': 'GR'
                }
        response = client.post('/add-expense/', data)
        data = {'location': 'coffee',
                'cost': '40.99',
                'transaction_date': '2015-09-09',
                'category': 'GR'
                }
        response = client.post('/add-expense/', data, follow=True)
        response = client.get(self.url)
        self.assertEqual(len(response.context['expenses']), 2)

    def test_expense_page_display_doesnt_display_other_user_expenses(self):
        user = self.create_user('tim', perm=True)
        client = self.login(user)

        data = {'location': 'Loblaws',
                'cost': '40.99',
                'transaction_date': '2015-09-09',
                'category': 'GR'
                }
        response = client.post('/add-expense/', data)
        client.logout()

        victoria = self.create_user('victoria', perm=True)
        client = self.login(victoria)

        data = {'location': 'Loblaws',
                'cost': '40.99',
                'transaction_date': '2015-09-09',
                'category': 'GR'
                }
        response = client.post('/add-expense/', data)
        response = client.get(self.url)

        self.assertEqual(len(response.context['expenses']), 1)

    def test_expense_page_displays_all_expense_costs(self):
        user = self.create_user('tim', perm=True)
        client = self.login(user)

        data = {'location': 'Loblaws',
                'cost': '14',
                'transaction_date': '2015-09-09',
                'category': 'GR'
                }
        response = client.post('/add-expense/', data)

        response = client.get(self.url)

        self.assertIn('14', response.content.decode())
