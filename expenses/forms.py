from allauth.utils import get_user_model
from django import forms
from django.core.exceptions import ValidationError
from django.contrib import messages

from expenses.models import Expense

SUCCESS_MSG = 'Success, new {category} expense: {location}, ${cost:,.2f}'

User = get_user_model()


class ExpenseForm(forms.models.ModelForm):

    def __init__(self, user=None, *args, **kwargs):
        super(ExpenseForm, self).__init__(*args, **kwargs)
        self.owner = user

    share_with = forms.ModelChoiceField(
        queryset=User.objects.filter(is_staff=False).all(),
        required=False
    )
    payer = forms.ModelChoiceField(
        queryset=User.objects.filter(is_staff=False).all(),
        required=False
    )

    class Meta:
        model = Expense
        fields = [
            'location',
            'cost',
            'transaction_date',
            'category',
            'description',
        ]
        widgets = {
                'location': forms.TextInput(),
        }

    def clean(self):
        cleaned_data = super(ExpenseForm, self).clean()
        shared = cleaned_data.get('share_with')
        payer = cleaned_data.get('payer')
        owner = self.owner
        if payer is not None:
            if shared is None and payer != owner:
                raise ValidationError("Only set payer on shared expenses")
        return cleaned_data

    def save(self, request):
        location = self.cleaned_data['location']
        cost = self.cleaned_data['cost']
        category = self.cleaned_data['category']
        owner = self.owner
        payer = self.cleaned_data.get('payer', owner)
        if not payer:
            payer = owner
        _expense = Expense.create_new(
            location=location,
            cost=cost,
            category=category,
            transaction_date=self.cleaned_data['transaction_date'],
            owner=owner,
            share_with=self.cleaned_data.get('share_with', None),
            payer=payer,
            description=self.cleaned_data.get('description','')
        )
        messages.success(request, SUCCESS_MSG.format(
            location=_expense.location,
            category=_expense.get_category_display(),
            cost=_expense.cost
        ))
