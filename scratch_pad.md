# Next steps
* deploy process is unclear for both production and local/staging, should fix & document to make more clear
* test_allauth - logout_user could do better job of waiting for nav to be exposed
* add notes to deployment/provision about creating secret.txt



# Notes
* test_home_page_can_save_post in expenses/tests.py is testing too many things 
* home_page view get latest seems odd using the id field, adding a date field would be good
* create page helper classes for functional test - https://github.com/hjwp/book-example/blob/chapter_21/functional_tests/home_and_list_pages.py

## Provision/Deploy Improvements
* adjust local deploy proceedure
	* use ansible to deploy to prod, setting env variables
		* https://developers.openshift.com/en/managing-environment-variables.html
		* http://stackoverflow.com/questions/18386759/django-on-openshift-keeping-config-files-different-in-different-environments


